<?php
namespace Core\Http;

use \Exception;
use Core\Http\Response;

class Router
{
    protected static $routes = [];

    public static function setRoutes(string $routesPath)
    {
        self::$routes = include $routesPath;
    }

    /**
     * @param Request|null $request
     * @return \Core\Http\Response
     * @throws Exception
     */
    public static function dispatch(?Request $request): Response
    {
        $handler = self::$routes[$request->path()] ?? null;

        if (is_null($handler)) {
            throw new Exception("404 not found");
        }

        $response = call_user_func_array($handler, [$request, new Response()]);

        if (! $response instanceof Response) {
            return (new Response())->send($response);
        }

        return $response;
    }
}