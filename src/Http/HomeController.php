<?php

namespace App\Http;

use App\Models\User;
use Core\Database\QueryBuilder;
use Core\Http\{Request, Response};

class HomeController
{
    public function index(Request $request, Response $response)
    {
        return User::query()->select(['id'])->where('id', 1)->toSql();
    }
}