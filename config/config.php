<?php

return [
    'app' => [
        'session_save_path' => '/cache/sessions',
        'view_path' => '/views'
    ],
    'database' => [
        'driver' => 'pgsql',
        'host' => 'localhost',
        'port' => 54320,
        'user' => 'homestead',
        'password' => 'secret',
        'dbname' => 'framework'
    ]
];